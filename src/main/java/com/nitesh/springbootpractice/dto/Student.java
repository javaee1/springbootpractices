package com.nitesh.springbootpractice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
class Address {
	private String city;
	private String state;
	private String country;


	@Override
	public String toString() {
		return "Address [city=" + city + ", state=" + state + ", country=" + country + "]";
	}
	
}


@Getter
@Setter
@NoArgsConstructor
public class Student {

	private String name;
	private Integer age;
	private Address address;
	
	
	
	public Student(String name, Integer age) {
		this.name = name;
		this.age = age;
		this.address = new Address("Dumka", "Jharkhand", "India");
	}


	@Override
	public String toString() {
		return "Student [name=" + name + ", age=" + age + ", address=" + address + "]";
	}
	
}

