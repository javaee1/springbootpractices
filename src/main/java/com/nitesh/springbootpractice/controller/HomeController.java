package com.nitesh.springbootpractice.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nitesh.springbootpractice.dto.Student;

import lombok.NonNull;

@RestController
@RequestMapping(HomeController.BASE_URL)
public class HomeController {
	
	public static final String BASE_URL = "/api/v1";
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@GetMapping("/hello")
	public Student welcomeMessage() {
		
		var student = new Student("Nitesh Nandan", 25);
		return student;
	}
	
	@PostMapping
	public String getPostRequest(@RequestBody Student student) {
		
		logger.info("Student " + student);
		
		return "OK";
		
	}

}
