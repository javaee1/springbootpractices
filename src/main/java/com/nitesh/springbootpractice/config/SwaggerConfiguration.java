package com.nitesh.springbootpractice.config;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class SwaggerConfiguration {
	
	private static final Logger logger = LoggerFactory.getLogger(SwaggerConfiguration.class);
	@Bean
	public Docket productApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.nitesh.springbootpractice"))
				.paths(PathSelectors.regex("/api.*"))
				.build()
				.apiInfo(metaInfo());
	}

	private ApiInfo metaInfo() {

        ApiInfo apiInfo = new ApiInfo(
                "Reporting for TextLocal",
                "Reporting for TextLocal Dummy",
                "1.0",
                "Terms of Service",
                new Contact("IMImobile", "https://www.textlocal.in/",
                        "talk2nandan5686@gmail.com"),
                "IMIMobile",
                "https://www.textlocal.in/", new ArrayList<>()
        );
        return apiInfo;
    }
}
